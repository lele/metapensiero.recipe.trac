==========================
 metapensiero.recipe.trac
==========================

Setup a full-featured Trac instance via buildout
================================================

This recipe allows you to automate the creation and management of multiples Trac instances. It
provides sensible default to the ``trac.ini`` configuration file and adds plugins to increase
Trac features. It also takes care of instance upgrades, Wiki documentation updates and source
code repository synchronization.

This package was originally part of the `Expert Python Programming` book written
by Tarek Ziadé, and it's actually a fork of the ``pbp.recipe.trac`` package v0.4.4dev\ [#]_.

.. contents::


Supported options
-----------------

The recipe supports the following options:

``project-name`` (required)
  Name of the Trac instance. This name will also be used as the default value
  for the ``smtp-from-name`` option. Default value: ``My project``.

``project-description``
  Description of the project. This description will also be used as the
  alternative text to the header logo.

``project-url``
  This URL will be used as the link on the header logo. Default value:
  ``http://example.com``.

``repos``
  This parameter list all the code repositories managed by your Trac instance.
  The syntax must respect the following scheme: ``Name | Type | Path | URL``.
  Name must be alphanumerical lower string.

  Supported values for types are: ``svn`` for Subversion, ``darcs`` for Darcs,
  ``git`` for Git.

  Path must point to the location of your code repository in the file system.

  The URL is not mandatory.

  For ``darcs`` and ``git`` repositories , there may be a fifth field, the URL
  of the master repository: if not already present at the specified path it
  will be cloned with a ``darcs get`` (or ``git clone --mirror``). In any
  case, when repos-sync is set to ``hook``, the repository will be eventually
  instrumented to execute the sync at apply/pull time. The field may be the
  value ``=`` if it is equal to the URL given in the fourth field.

  For ``git`` repositories there may be a sixth field, either the name or a
  pathname of the `reference` repository: this may be useful when cloning
  several related repositories to reduce the amount of data that needs to be
  downloaded as well as the local disk space used by the clones.

  You can add several repositories, one per line. Example::

    repos =
      repo1 | svn   | /data/svn/my_reposo                    | http://my-domain.net/subversion/repo1
      repo2 | git   | ${buildout:directory}/repos/lele.git   | https://bitbucket.org/lele/metapensiero.recipe.trac.git     | git@bitbucket.org:lele/metapensiero.recipe.trac.git
      repo3 | git   | ${buildout:directory}/repos/azazel.git | https://bitbucket.org/azazel75/metapensiero.recipe.trac.git | = | repo2
      repo4 | darcs | /data/darcs/reposo2                    | http://darcs.stuff.com/project                              | stuff.com:/master/project

  where you can see that the ``repo2`` is cloned from the SSH account on
  bitbucket.org but Trac will show its HTTPS URL, while ``repo3`` is cloned
  from via HTTPS using the local ``repo2`` repository as a reference.

``default-repo``
  If set with a repository name, this will create an alias from the default
  repository to the given repository. Example::

    default-repo = repo2

``repos-sync``
  This option deals with the way repositories are synchronized with Trac.

  Currently it only support `per-request synchronization
  <http://trac.edgewall.org/wiki/TracRepositoryAdmin#Synchronization>`_. This is
  the method that will be setup if you give this option the ``request`` value.
  This is the default value.

  Currently only for ``darcs`` repositories, this option accepts ``hook`` as a value,
  to setup `explicit synchronization
  <http://trac.edgewall.org/wiki/TracRepositoryAdmin#ExplicitSync>`_.

``force-instance-upgrade``
  If set to ``True``, this will trigger the internal Trac upgrade process on
  the current instance, even if Trac don't think it's needed. Default value:
  ``False``.

``force-repos-resync``
  If set to ``True``, this will force the resynchronization of Trac against all
  the repositories. Default value: ``False``.

``wiki-doc-upgrade``
  If set to ``True``, this will upgrade the default wiki pages embedded in the
  current Trac instance. As said in Trac documentation, this `will not remove
  deprecated wiki pages
  <http://trac.edgewall.org/wiki/TracUpgrade#WikiUpgrade>`_ that were previously
  part of a Trac release. Default value: ``False``.

``remove-examples``
  If set to ``False``, this will not remove the default milestones and
  components added by Trac when creating a brand new instance. Default value:
  ``True``.

``milestones``
  List of all custom milestones to create. You can add several custom
  parameters, one per line. Example::

    milestones = 0.1
                 0.2
                 1.0
                 Future
                 Undecided

``components``
  List of components for which we can attached Trac tickets to. The syntax must
  follow the ``Component name | Component owner`` scheme, but components without
  an owner are allowed. Example::

    components = The application itself | kevin
                 Project web site
                 Build tools            | cecile

``permissions``
  List of custom permissions to set. Both users and groups are supported.
  Example::

    permissions = cecile        | REPORT_ADMIN
                  kevin         | PERMISSION_ADMIN MILESTONE_ADMIN REPORT_ADMIN
                  anonymous     | STATS_VIEW
                  authenticated | REPORT_MODIFY MILESTONE_MODIFY

  A `list of permissions <http://trac.edgewall.org/wiki/TracPermissions>`_ can
  be found on Trac's wiki. In respects to Trac's conventions, all permissions
  are automatically upper-cased by the recipe.

  To revoke a list of permissions, add a third field ``revoke``::

    permissions = authenticated | WIKI_CREATE | revoke

``header-logo``
  Location of the logo that will replace the default Trac logo at the top of
  each page. The file will be copied by the recipe to the ``htdocs`` directory
  of your Trac instance.

``footer-message``
  The bottom right-aligned text displayed on each page displayed by Trac. HTML
  can be used here. Default value ``This Trac instance was generated by
  <a href="http://pypi.python.org/pypi/pbp.recipe.trac">pbp.recipe.trac</a>.``.

``smtp-always-bcc``
  Email address(es) to always send notifications to, addresses do not appear
  publicly (Bcc:).

``smtp-always-cc``
  Email address(es) to always send notifications to, addresses can be seen by
  all recipients (Cc:).

``smtp-default-domain``
  Default host/domain to append to address that do not specify one

``smtp-enabled``
  Enable SMTP (email) notification.

``smtp-from``
  Sender address to use in notification emails.

``smtp-from-name``
  Sender name to use in notification emails. Default value: ``project-name``
  option value.

``smtp-password``
  Password for SMTP server.

``smtp-port``
  SMTP server port to use for email notification.

``smtp-replyto``
  Reply-To address to use in notification emails.

``smtp-server``
  SMTP server hostname to use for email notifications.

``smtp-subject-prefix``
  Text to prepend to subject line of notification emails. If the setting is not
  defined, then the value of ``project-name`` is used as prefix. If no prefix
  is desired,then specifying an empty option will disable it.

``smtp-user``
  Username for SMTP server.

``time-tracking-plugin``
  If set to ``enabled``, will activate the `Estimation and Time Tracking plugin
  <http://trac-hacks.org/wiki/TimingAndEstimationPlugin>`_. Default value:
  ``disabled``.

``stats-plugin``
  If set to ``enabled``, will activate the `TracStats plugin
  <http://trac-hacks.org/wiki/TracStatsPlugin>`_. Default value: ``disabled``.

``acctmgr-plugin``
  If set to ``enabled``, will activate the `Account Manager plugin
  <http://trac-hacks.org/wiki/AccountManagerPlugin>`_. Default value:
  ``disabled``.

``password-file``
  Pathname of the htaccess password file.

``trac-ini-additional``
  In case a Trac parameter is not natively supported by this recipe, you can
  use this to add your own. The syntax must respect the following scheme:
  ``Section | Parameter | Value``. You can add several custom parameters, one
  per line. Example::

    trac-ini-additional = attachment   | max_size            | 52428800
                          notification | always_notify_owner | true
                          logging      | log_level           | DEBUG

  This option is applied just before writing the final ``trac.ini`` generated by
  this recipe. So thanks to ``trac-ini-additional``, you always have a way to
  fix your ``trac.ini`` even if this recipe breaks it.

  And to get more informations on all the ``trac.ini`` parameters, see:
  http://trac.edgewall.org/wiki/TracIni

``default-charset``
  Default to "utf-8".

``parent-directory``
  Default to the ``parts-directory``.

``user``
  If specified, the recipe will change ownership of the following
  files to the given user:

  * instance configuration (``.../conf/trac.ini``)
  * the database (``.../db/trac.db``) and its directory
  * the log directory (``.../log``)
  * the attachments directory (``.../attachments``)
  * the ``password-file``, if specified

``ticket-workflow``
  If specified, it may be either a complete ``[ticket-workflow]`` section
  or the path of the file containing such section. Example::

    ticket-workflow = path/to/trac/contrib/workflow/enterprise-workflow.ini


TracDarcs specific options
~~~~~~~~~~~~~~~~~~~~~~~~~~

``darcs-command``
  Set the ``darcs`` executable, defaults to "darcs".

``darcs-dont-escape-8bit``
  Controls the ``DARCS_DONT_ESCAPE_8BIT`` setting, default to "false".

``darcs-possible-encodings``
  Determine which codecs will be tried when loading ``darcs`` changes,
  default to "utf-8,iso8859-1".

``max-concurrent-darcses``
  Max number of concurrent darcses running at the same time. Default to 0,
  ie no limit.

``darcs-eager-annotations``
  Default to "false".

``tracpy``
  Python interpreter used by the `explicit synchronization`_ hook.
  Default to "bin/trac-py".


Git specific options
~~~~~~~~~~~~~~~~~~~~

``git-cached-repository``
  Default to "false".

``git-persistent-cache``
  Default to "false".

``git-shortrev-len``
  Default to 7.

``git-wiki-shortrev-len``
  Default to 7.


Example usage
-------------

We'll start by creating a buildout that uses the recipe::

    >>> write('buildout.cfg',
    ... """
    ... [buildout]
    ... parts = trac
    ... index = http://pypi.python.org/simple
    ...
    ... [trac]
    ... recipe = metapensiero.recipe.trac
    ... project-name = My project
    ... project-url = http://example.com
    ... trac-ini-additional =
    ...   trac | repository_type | hg
    ...   trac | repository_dir | sqlite:${buildout:directory}/var/test.hg
    ... header-logo = ${buildout:directory}/my_logo
    ... smtp-server = localhost
    ... smtp-port = 25
    ... smtp-from = tarek@ziade.org
    ... smtp-replyto = tarek@ziade.org
    ... """)

Let's run the buildout::

    >>> res = system(buildout)

This creates a trac instance::

    >>> ls(join(sample_buildout, 'parts', 'trac'))
    -  README
    -  VERSION
    d  conf
    d  db
    d  htdocs
    d  log
    d  plugins
    d  templates

With a trac.ini file. Let's check its content::

    >>> f = join(sample_buildout, 'parts', 'trac', 'conf', 'trac.ini')
    >>> from ConfigParser import ConfigParser
    >>> parser = ConfigParser()
    >>> null = parser.read([f])
    >>> parser.get('trac', 'repository_type')
    'hg'
    >>> parser.get('trac', 'repository_dir')
    '/sample-buildout/var/test.hg'
    >>> parser.get('project', 'descr')
    'My example project'
    >>> parser.get('project', 'name')
    'My project'
    >>> parser.get('project', 'url')
    ''


Contributors
------------

- `Tarek Ziade <http://ziade.org>`_, Author [tarek]

- `Kevin Deldycke <http://kevin.deldycke.com>`_, Contributor [kdeldycke]

- Marco Scheidhuber, Contributor [j23d]

- Lele Gaifax, Current maintainer


.. [#] Original references:

       - Documentation: http://pypi.python.org/pypi/pbp.recipe.trac

       - Bug tracker: http://bitbucket.org/tarek/atomisator/issues

       - Source: http://bitbucket.org/tarek/atomisator/src/tip/packages/pbp.recipe.trac/

       - Tutorial, example and how-to migrate from a legacy Trac instance:
         http://kevin.deldycke.com/2010/12/automate-trac-instance-deployment-buildout/

       - Page about Buildout-based installation on Trac wiki:
         http://trac.edgewall.org/wiki/CookBook/TracBuildout

       - pbp.recipe.trac is a sub-project of Atomisator: http://atomisator.ziade.org
