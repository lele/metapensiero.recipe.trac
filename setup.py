# -*- coding: utf-8 -*-
"""
This module contains the tool of metapensiero.recipe.trac
"""

from codecs import open
import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.rst'), encoding='utf-8') as f:
    CHANGES = f.read()
with open(os.path.join(here, 'version.txt'), encoding='utf-8') as f:
    VERSION = f.read().strip()

entry_points = {
    "zc.buildout": [
        "default = metapensiero.recipe.trac:Recipe",
        "wsgi = metapensiero.recipe.trac.wsgi:Recipe"
    ]
}

requires = ['Trac', 'setuptools', 'zc.buildout']
tests_require = ['zope.testing', 'manuel']

setup(name='metapensiero.recipe.trac',
      version=VERSION,
      description="ZC Buildout recipe to install and configure a Trac server.",
      long_description=README + u'\n\n' + CHANGES,
      # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        'Framework :: Buildout',
        'Framework :: Buildout :: Recipe',
        'Framework :: Trac',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        ],
      keywords='trac metapensiero buildout recipe',
      author='Tarek Ziade',
      author_email='tarek@ziade.org',
      maintainer='Lele Gaifax',
      maintainer_email='lele@metapensiero.it',
      url='https://bitbucket.org/lele/metapensiero.recipe.trac',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['metapensiero', 'metapensiero.recipe'],
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=tests_require,
      extras_require=dict(tests=tests_require),
      test_suite='metapensiero.recipe.trac.test.suite',
      entry_points=entry_points,
      )
