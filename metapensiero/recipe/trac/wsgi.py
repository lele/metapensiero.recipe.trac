# -*- coding: utf-8 -*-
# :Progetto:  metapensiero.recipe.trac -- wsgi recipe
# :Creato:    gio 09 dic 2010 12:03:57 CET
# :Autore:    Lele Gaifax <lele@nautilus.homeip.net>
# :Licenza:   GNU General Public License version 3 or later
#

import os
from zc.recipe.egg.egg import Eggs

WRAPPER_TEMPLATE = """\
# -*- coding: utf-8 -*-

import os
import sys

syspaths = [
    %(syspath)s,
    ]

for path in reversed(syspaths):
    if path not in sys.path:
        sys.path[0:0]=[path]

def application(environ, start_request):
    %(env_path_slot)s
    %(index_template_slot)s
    from trac.web.main import dispatch_request
    return dispatch_request(environ, start_request)

%(initialization)s
"""

class Recipe:
    def __init__(self, buildout, name, options):
        self.buildout = buildout
        self.name = name
        self.options = options

    def install(self):
        egg = Eggs(self.buildout, self.options["recipe"], self.options)
        reqs, ws = egg.working_set()
        path = [pkg.location for pkg in ws]
        extra_paths = self.options.get('extra-paths', '')
        extra_paths = extra_paths.split()
        path.extend(extra_paths)

        parent_dir = self.options.get('parent-directory')
        if parent_dir is not None:
            env_path_slot = "environ['trac.env_parent_dir'] = %s" % repr(parent_dir)
        else:
            env_dir = self.options.get('env-directory')
            env_path_slot = "environ['trac.env_path'] = %s" % repr(env_dir)

        template = self.options.get('index-template')
        if template is not None:
            template_slot = "environ['trac.env_index_template'] = %s" % repr(template)
        else:
            template_slot = ""

        initialization = self.options.get('initialization', '')

        output = WRAPPER_TEMPLATE % dict(
            env_path_slot=env_path_slot,
            index_template_slot=template_slot,
            initialization=initialization,
            syspath=",\n    ".join((repr(p) for p in path))
            )

        target = self.options.get('target')
        if target is None:
            location = os.path.join(self.buildout["buildout"]["parts-directory"],
                                    self.name)
            if not os.path.exists(location):
                os.mkdir(location)
                self.options.created(location)
            target = os.path.join(location, "wsgi")

        with open(target, "wt") as f:
            f.write(output)

        os.chmod(target, 0755)
        self.options.created(target)

        return self.options.created()

    update = install
