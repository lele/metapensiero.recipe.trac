# -*- coding: utf-8 -*-
"""Recipe trac"""

import os
import sys
import subprocess
import ConfigParser
import shutil
import string

import pkg_resources
import zc.buildout

from trac.admin.console import TracAdmin
from trac.ticket.model import Component, Milestone, Version
from trac.perm import PermissionSystem
from trac.versioncontrol import RepositoryManager
from trac.wiki.admin import WikiAdmin
from trac.resource import ResourceNotFound
from trac.util import as_bool


GIT_POST_RECEIVE_HOOK = """\
#!/bin/sh
while read oldrev newrev refname
do
  git rev-list --reverse $newrev ^$oldrev | while read rev
    do
      %(tracadmin)s %(envdir)s changeset added %(repo_dir)s $rev
    done
done
"""

def make_id(s, _valid_chars=set(string.ascii_letters + string.digits + "-._~")):
    "Transform any string to an ID"

    if s is None:
        return None
    else:
        return ''.join([c for c in s if c in _valid_chars]).lower()

def clean_multi_params(v, maxsplit=-1):
    "Parse a multi-line/multi-value parameter"

    cleaned_params = []
    if v is not None:
        for line in v.split('\n'):
            line = line.strip()
            if line:
                cleaned_params.append([f.strip() for f in line.split('|', maxsplit)])
    return cleaned_params


class Recipe(object):
    """zc.buildout recipe"""

    def __init__(self, buildout, name, options):
        self.buildout, self.name, self.options = buildout, name, options
        parent_dir = options.get('parent-directory')
        if parent_dir is None:
            parent_dir = buildout['buildout']['parts-directory']
        options['location'] = os.path.join(parent_dir, self.name)
        options['bin-directory'] = buildout['buildout']['bin-directory']
        options['executable'] = sys.executable

    def install(self):
        """Installer"""

        options = self.options

        # Add command line scripts trac-admin and tracd into bin
        entry_points = [('trac-admin', 'trac.admin.console', 'run'),
                        ('tracd', 'trac.web.standalone', 'main')]
        zc.buildout.easy_install.scripts(
            entry_points, pkg_resources.working_set,
            options['executable'], options['bin-directory']
        )


        ####################
        # Init Trac instance
        ####################

        # Generate the trac instance, if required
        location = options['location']
        project_name = options.get('project-name', 'My project')
        project_url = options.get('project-url', 'http://example.com')
        db = 'sqlite:%s' % os.path.join('db', 'trac.db')
        if not os.path.exists(location):
            os.makedirs(location)
        trac = TracAdmin(location)
        if not trac.env_check():
            trac.do_initenv('"%s" %s' % (project_name, db))
            needs_repo_resync = True
        else:
            # Instance already exists, upgrade it if needed
            needs_repo_resync = False
            needs_upgrade = trac.env.needs_upgrade()
            if needs_upgrade:
                trac.env.upgrade(backup=True)

        user_name = options.get('user')
        if user_name:
            import pwd
            try:
                user = pwd.getpwnam(user_name)
            except KeyError:
                pass
            else:
                user_id = user.pw_uid
                group_id = user.pw_gid
                try:
                    os.chown(os.path.join(location, 'conf', 'trac.ini'), user_id, group_id)
                    os.chown(os.path.join(location, 'db'), user_id, group_id)
                    os.chown(os.path.join(location, 'db', 'trac.db'), user_id, group_id)
                    os.chown(os.path.join(location, 'log'), user_id, group_id)
                    os.chown(os.path.join(location, 'attachments'), user_id, group_id)
                except OSError:
                    pass
                password_file = options.get('password-file')
                if password_file:
                    if not os.path.exists(password_file):
                        # Touch it
                        pwdf = open(password_file, 'w')
                        pwdf.close()
                    try:
                        os.chown(password_file, user_id, group_id)
                    except OSError:
                        pass

        env = trac.env

        # Remove Trac default example data
        clean_up = as_bool(options.get('remove-examples', 'True'))
        if clean_up:
            # Remove default milestones
            for mil in Milestone.select(env):
                if mil.name in ['milestone1', 'milestone2', 'milestone3', 'milestone4']:
                    mil.delete()
            # Remove default components
            for comp in Component.select(env):
                if comp.name in ['component1', 'component2']:
                    comp.delete()
            # Remove default versions
            for ver in Version.select(env):
                if ver.name in ['1.0', '2.0']:
                    ver.delete()

        # Add custom milestones
        for mil_name in clean_multi_params(options.get('milestones', '')):
            try:
                mil = Milestone(env, name=mil_name)
            except ResourceNotFound:
                mil = Milestone(env)
                mil.name = mil_name
                mil.insert()

        # Add custom components
        for comp_data in clean_multi_params(options.get('components', '')):
            comp_name = comp_data[0]
            comp_owner = comp_data[1] if len(comp_data)>1 else None
            try:
                comp = Component(env, name=comp_name)
            except ResourceNotFound:
                comp = Component(env)
                comp.name = comp_name
                comp.owner = comp_owner
                comp.insert()

        # Add custom versions
        for ver_data in clean_multi_params(options.get('versions', '')):
            ver_name = ver_data[0]
            ver_owner = ver_data[1] if len(ver_data)>1 else None
            try:
                ver = Version(env, name=ver_name)
            except ResourceNotFound:
                ver = Version(env)
                ver.name = ver_name
                ver.owner = ver_owner
                ver.insert()


        #######################
        # Generate the trac.ini
        #######################

        # Read the trac.ini config file
        trac_ini = os.path.join(location, 'conf', 'trac.ini')
        parser = ConfigParser.ConfigParser()
        parser.read([trac_ini])

        # Clean-up trac.ini: add missing stuff
        if 'components' not in parser.sections():
            parser.add_section('components')

        # Force upgrade of informations used during initialization
        parser.set('project', 'name', project_name)
        parser.set('project', 'url', project_url)
        parser.set('trac', 'base_url', project_url)

        # Set default encoding
        parser.set('trac', 'default_charset', options.get('default-charset', 'utf-8'))

        # Set all repositories
        repos = clean_multi_params(options.get('repos', None))
        repo_names = [make_id(r[0]) for r in repos]
        repo_types = set([r[1].lower() for r in repos])
        if 'repositories' not in parser.sections():
            parser.add_section('repositories')

        # Set repository sync method
        sync_method = options.get('repos-sync', 'request').strip().lower()
        svn_repos = [make_id(r[0]) for r in repos if r[1] == 'svn']
        if sync_method == 'request':
            parser.set('trac', 'repository_sync_per_request', ', '.join(svn_repos))
        else:
            parser.set('trac', 'repository_sync_per_request', '')

        tracpy = options.get('tracpy')
        if tracpy is None:
            tracpy = os.path.join(self.buildout['buildout']['bin-directory'], 'trac-py')
        tracadmin = os.path.join(self.buildout['buildout']['bin-directory'], 'trac-admin')
        repo_dirs = {}
        for repo in repos:
            repo_name = make_id(repo[0])
            repo_type = repo[1]
            repo_dir  = repo[2]
            repo_url  = repo[3]
            repo_dirs[repo_name] = repo_dir
            parser.set('repositories', '%s.type' % repo_name, repo_type)
            parser.set('repositories', '%s.dir'  % repo_name, repo_dir)
            if repo_url not in ['', None]:
                parser.set('repositories', '%s.url'  % repo_name, repo_url)

            # Handle sync_method=='hook' for darcs
            if repo_type == 'darcs' and len(repo)>4:
                master_url = repo_url if repo[4]=='=' else repo[4]
                if not os.path.exists(repo_dir):
                    parent_dir = os.path.dirname(repo_dir)
                    if not os.path.exists(parent_dir):
                        os.mkdir(parent_dir)
                    subprocess.call(['darcs', 'get', master_url, repo_dir])
                if sync_method == 'hook':
                    defaults_file = os.path.join(repo_dir, '_darcs', 'prefs', 'defaults')
                    if os.path.exists(defaults_file):
                        defaults = open(defaults_file).readlines()
                    else:
                        defaults = []
                    already_done = any([l.startswith('apply posthook ') for l in defaults])
                    if not already_done:
                        post_hook = "posthook %s %s changeset added $(pwd) $(%s -m tracdarcs.changesparser)" % (
                            tracadmin, location, tracpy)
                        defaults.append("apply " + post_hook + "\n")
                        defaults.append("apply run-posthook\n")
                        defaults.append("pull " + post_hook + "\n")
                        defaults.append("pull run-posthook\n")
                        with open(defaults_file, 'w') as f:
                            f.writelines(defaults)
            elif repo_type == 'git' and len(repo) > 4:
                master_url = repo_url if repo[4]=='=' else repo[4]
                ref_path = None if len(repo) == 5 else repo[5]
                if not os.path.exists(repo_dir):
                    parent_dir = os.path.dirname(repo_dir)
                    if not os.path.exists(parent_dir):
                        os.mkdir(parent_dir)
                    gitcmd = ['git', 'clone', '--mirror']
                    if ref_path is not None:
                        gitcmd.extend(['--reference', repo_dirs.get(ref_path, ref_path)])
                    gitcmd.extend([master_url, repo_dir])
                    subprocess.call(gitcmd)
                if sync_method == 'hook':
                    post_receive_hook_file = os.path.join(repo_dir, 'hooks', 'post-receive')
                    with open(post_receive_hook_file, 'w') as f:
                        f.write(GIT_POST_RECEIVE_HOOK % {
                            'envdir': location,
                            'tracadmin': tracadmin,
                            'repo_dir': repo_dir
                        })
                    os.chmod(post_receive_hook_file, 0775)

        # Set default repository
        default_repo = make_id(options.get('default-repo', None))
        if default_repo and default_repo in repo_names:
            parser.set('repositories', '.alias', default_repo)
            # If there is only one repository, show only the alias and
            # hide the concrete one, otherwise hide the alias
            if len(repo_names) == 1:
                parser.set('repositories', '.hidden', 'false')
                parser.set('repositories', '%s.hidden' % repo_names[0], 'true')
            else:
                parser.set('repositories', '.hidden', 'true')

        # Set project description
        project_descr = options.get('project-description', None)
        if project_descr:
            parser.set('project', 'descr', project_descr)
            parser.set('header_logo', 'alt', project_descr)

        # Setup logo
        header_logo = options.get('header-logo', '')
        if header_logo:
            header_logo = os.path.realpath(header_logo)
            if os.path.exists(header_logo):
                shutil.copyfile(header_logo, os.path.join(location, 'htdocs', 'logo'))
            parser.set('header_logo', 'src', 'site/logo')
        else:
            parser.set('header_logo', 'src', 'common/trac_banner.png')
        parser.set('header_logo', 'link', project_url)

        # Set footer message
        parser.set('project', 'footer', options.get('footer-message', 'This Trac instance was generated by <a href="http://pypi.python.org/pypi/pbp.recipe.trac">pbp.recipe.trac</a>.'))

        # SMTP parameters
        for name in ('always-bcc', 'always-cc', 'default-domain', 'enabled',
                     'from', 'from-name', 'password', 'port', 'replyto',
                     'server', 'subject-prefix', 'user'):
            param_name = "smtp-%s" % name
            default_value = None
            if param_name == "smtp-from-name":
                default_value = project_name
            value = options.get(param_name, default_value)
            if value is not None:
                parser.set('notification', param_name.replace('-', '_'), value)

        # Ticket workflow
        ticket_workflow = options.get('ticket-workflow', '')
        if ticket_workflow:
            parser.remove_section('ticket-workflow')
            if not '\n' in ticket_workflow:
                ticket_workflow_fn = os.path.realpath(ticket_workflow)
                if os.path.exists(ticket_workflow_fn):
                    parser.read(ticket_workflow_fn)
            else:
                from cStringIO import StringIO
                wf = StringIO(ticket_workflow)
                parser.readfp(wf)


        ###############
        # Plugins setup
        ###############

        # If no repository uses Subversion, disable its component
        if not 'svn' in repo_types:
            parser.set('components', 'trac.versioncontrol.svn_fs.*', 'disabled')

        # If one repository use Darcs, hook its plugin
        if 'darcs' in repo_types:
            parser.set('components', 'tracdarcs.*', 'enabled')
            if 'darcs' not in parser.sections():
                parser.add_section('darcs')
            parser.set('darcs', 'command', options.get('darcs-command', 'darcs'))
            parser.set('darcs', 'dont_escape_8bit', options.get('darcs-dont-escape-8bit', 'false'))
            parser.set('darcs', 'possible_encodings', options.get('darcs-possible-encodings', 'utf-8,iso8859-1'))
            parser.set('darcs', 'max_concurrent_darcses', options.get('max-concurrent-darcses', '0'))
            parser.set('darcs', 'eager_annotations', options.get('darcs-eager-annotations', 'false'))

        # If one repository use Git, hook its plugin
        if 'git' in repo_types:
            parser.set('components', 'tracopt.versioncontrol.git.*', 'enabled')
            if 'git' not in parser.sections():
                parser.add_section('git')
            parser.set('git', 'cached_repository', options.get('git-cached-repository', 'false'))
            parser.set('git', 'persistent_cache', options.get('git-persistent-cache', 'false'))
            parser.set('git', 'shortrev_len', options.get('git-shortrev-len', '7'))
            parser.set('git', 'wiki_shortrev_len', options.get('git-wiki-shortrev-len', '7'))

        # Enable and setup time tracking
        time_tracking = options.get('time-tracking-plugin', 'disabled').strip().lower() == 'enabled'
        if time_tracking:
            parser.set('components', 'timingandestimationplugin.*', 'enabled')

        # Enable and setup the stat plugin
        stats = options.get('stats-plugin', 'disabled').strip().lower() == 'enabled'
        if stats:
            parser.set('components', 'tracstats.*', 'enabled')

        # Enable and setup the account manager plugin
        acctmgr = options.get('acctmgr-plugin', 'disabled').strip().lower() == 'enabled'
        if acctmgr:
            parser.set('components', 'acct_mgr.admin.*', 'enabled')
            parser.set('components', 'acct_mgr.api.*', 'enabled')
            parser.set('components', 'acct_mgr.db.*', 'enabled')
            parser.set('components', 'acct_mgr.htfile.*', 'enabled')
            parser.set('components', 'acct_mgr.http.*', 'enabled')
            parser.set('components', 'acct_mgr.pwhash.*', 'enabled')
            parser.set('components', 'acct_mgr.register.*', 'enabled')
            parser.set('components', 'acct_mgr.svnserve.*', 'enabled')
            parser.set('components', 'acct_mgr.web_ui.*', 'enabled')

            password_file = options.get('password-file')
            if password_file:
                if 'account-manager' not in parser.sections():
                    parser.add_section('account-manager')
                store = options.get('acctmgr-store', 'htpasswd')
                if store == 'htpasswd':
                    parser.set('account-manager', 'password_store', 'HtPasswdStore')
                    parser.set('account-manager', 'htpasswd_file', password_file)
                    parser.set('account-manager', 'htpasswd_hash_type', 'md5')
                elif store == 'htdigest':
                    parser.set('account-manager', 'password_store', 'HtDigestStore')
                    parser.set('account-manager', 'htdigest_file', password_file)


        #######################
        # Final upgrades & sync
        #######################

        # Apply custom parameters defined by the user
        custom_params = clean_multi_params(options.get('trac-ini-additional', ''),
                                           # Use 2 as maxsplit, to allow
                                           # the third component to
                                           # contain the pipe character
                                           # (as mime_map_patterns for
                                           # example)
                                           2)
        for param in custom_params:
            if len(param) == 3:
                section = param[0]
                if section not in parser.sections():
                    parser.add_section(section)
                parser.set(section, param[1], param[2])

        # Write the final trac.ini
        parser.write(open(trac_ini, 'w'))

        # Reload the environment
        env.shutdown()
        trac = TracAdmin(location)
        env = trac.env

        # Set custom permissions
        perm_sys = PermissionSystem(env)
        for cperm in clean_multi_params(options.get('permissions', '')):
            revoke_permissions = False
            if len(cperm) == 3:
                if cperm.pop().strip().lower() == 'revoke':
                    revoke_permissions = True
            if len(cperm) == 2:
                user = cperm[0]
                current_user_perms = perm_sys.get_user_permissions(user)
                perm_list = [p.upper() for p in cperm[1].split(' ') if len(p)]
                for perm in perm_list:
                    if revoke_permissions:
                        if perm in current_user_perms:
                            perm_sys.revoke_permission(user, perm)
                    else:
                        if perm not in current_user_perms:
                            perm_sys.grant_permission(user, perm)

        # Upgrade Trac instance to keep it fresh
        needs_upgrade = env.needs_upgrade()
        force_upgrade = as_bool(options.get('force-instance-upgrade', 'False'))
        if needs_upgrade or force_upgrade:
            env.upgrade(backup=True)

        # Force repository resync
        repo_resync = needs_repo_resync or as_bool(options.get('force-repos-resync', 'False'))
        if repo_resync:
            rm = RepositoryManager(env)
            repositories = rm.get_real_repositories()
            for repos in sorted(repositories, key=lambda r: r.reponame):
                repos.sync(clean=True)

        # Upgrade default wiki pages embedded in Trac instance
        wiki_upgrade = as_bool(options.get('wiki-doc-upgrade', 'False'))
        if wiki_upgrade:
            # Got the command below from trac/admin/console.py
            pages_dir = pkg_resources.resource_filename('trac.wiki',
                                                        'default-pages')
            WikiAdmin(env).load_pages( pages_dir
                                     , ignore=['WikiStart', 'checkwiki.py']
                                     , create_only=['InterMapTxt']
                                     )

        # Return files that were created by the recipe. The buildout
        # will remove all returned files upon reinstall.
        return tuple()

    update = install
